# Element Remover

A Firefox extension/add-on that allows you to right click anywhere on a webpage and remove the element that you are
 hovered over from being seen. Very helpful for instantly removing the ads and video players that are displayed on many websites. 

## Installation

- [Download The Extension From the Firefox Store](https://addons.mozilla.org/en-US/firefox/addon/page-element-remover/)

